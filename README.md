BioModels pipeline for Europe PMC External Links Service
========================================================

## Introduction

This library exports the BioModels mapping for EuropePMC's
[External Links Service](https://europepmc.org/LabsLink).

It makes it possible for BioModels backlinks to appear on EuropePMC when
a user looks in EuropePMC at a paper associated with a model hosted by us.

## Usage

Please note Java 8 is required.

### Build

The command

    ./gradlew check shadowJar

will run all tests and package the library into a jar with all needed dependencies.

### Run

Create a properties file (see src/test/resources/sample.properties for an example)
and then run

    java -jar build/libs/eupmc-links-biomodels*-uber.jar <properties_file>

this will produce the necessary files in the current working directory
* *profile.xml* : hosting BioModels profile information
* *links.xml.gz*: containing the mapping between BioModels submissions and their
corresponding manuscript.

## Copyright
&copy; 2019 European Molecular Biology Laboratory,
[GPL v3 license](https://www.gnu.org/licenses/gpl-3.0.en.html)

## Contact
biomodels-developers [at] lists [dot] sf [dot] net
