package net.biomodels.eupmc.links

import groovy.transform.CompileStatic
import net.biomodels.eupmc.links.config.Configuration
import net.biomodels.eupmc.links.output.DefaultOutputService
import net.biomodels.eupmc.links.retrieval.DefaultExtractionService

import java.nio.file.Path
import java.nio.file.Paths

@CompileStatic
class ModelPublicationMapper {
    final Configuration config
    final ExtractionService extractionService
    final OutputService outputService

    ModelPublicationMapper() {
        this(new String[0], new DefaultExtractionService(), new DefaultOutputService())
    }

    ModelPublicationMapper(String[] args) {
        this(args, new DefaultExtractionService(), new DefaultOutputService())
    }

    ModelPublicationMapper(String[] args, ExtractionService extractionService,
            OutputService outputService) {
        if (args.length == 0) {
            Configuration.parseSettings((Path) null) // error out of here
        }
        String configLocation = args[0]
        this.config = Configuration.parseSettings Paths.get(configLocation)
        this.extractionService = Objects.requireNonNull extractionService
        this.outputService     = Objects.requireNonNull(outputService)
    }

    boolean run() {
        boolean result = true
        try {
            def publications = extractionService.fetch config
            writePublicationLinks(publications)
        } catch (Exception e) {
            result = false
            System.err.println("Could not process model publications: $e")
        }
        result
    }

    private void writePublicationLinks(Collection<ModelPublicationDetails> publications) {
        outputService.writeProfileInformation config
        outputService.writeLinksInformation config, publications
    }
}
