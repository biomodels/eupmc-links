package net.biomodels.eupmc.links.retrieval

import groovy.sql.GroovyRowResult
import groovy.sql.Sql
import groovy.transform.CompileStatic
import net.biomodels.eupmc.links.ExtractionService
import net.biomodels.eupmc.links.ModelPublicationDetails
import net.biomodels.eupmc.links.config.Configuration

import java.sql.SQLException

/**
 * Simple {@link ExtractionService} implementation that retrieves publication information
 * from the database using the connection details from the supplied configuration.
 */
@CompileStatic
class DefaultExtractionService implements ExtractionService {
    public static final String PUBMED = 'PUBMED'
    /**
     *
     * @param config
     * @return
     * @throws NullPointerException if config is undefined
     */
    @Override
    Collection<ModelPublicationDetails> fetch(Configuration config) {
        def connection = config.database.asSqlMap()
/*        def connectionInfo = config.database.asPropMap()
        def connection = connectionInfo.driver.connect(connectionInfo.url, connectionInfo.properties)*/

        def sql = null
        try {
            sql = Sql.newInstance(connection)
            return doFetch(sql)
        } catch (SQLException | ClassNotFoundException e) {
            throw new RuntimeException(""" Error extracting model publication details
                using connection $connection with: $e""")
        } finally {
            sql?.close()
            //connection?.close()
        }
    }

    @SuppressWarnings(["GrMethodMayBeStatic", "SqlDialectInspection"])
    private Collection<ModelPublicationDetails> doFetch(Sql sql) {
        def results = sql.rows("""\
select
    concat(
        "http://identifiers.org/biomodels.db/",
        ifnull(model.perennialPublicationIdentifier, model.submission_id)
    ) as uri,
    publication.link as publicationId,
    publication_link_provider.link_type as linkType
from model
    join publication on model.publication_id = publication.id
    join publication_link_provider on publication_link_provider.id = publication.link_provider_id
where
    model.deleted = false
    /* Europe PMC ExternalLinksService only supports DOIs and PMIDs */
    AND link_type in ('PUBMED', 'DOI')
    AND model.id in (
select
    distinct model_id
from acl_entry
    join acl_object_identity on acl_object_identity.id = acl_entry.acl_object_identity
    join acl_class on acl_object_identity.object_id_class = acl_class.id
    join acl_sid on acl_entry.sid = acl_sid.id
    join revision on revision.id = acl_object_identity.object_id_identity
where
    acl_sid.sid ='ROLE_ANONYMOUS'
    AND acl_entry.mask = 1
    AND acl_class.class = 'net.biomodels.jummp.model.Revision'
)
""")
        results.collect { GroovyRowResult r ->
            convertToModelPublicationDetails(r)
        }
    }

    @SuppressWarnings("GrMethodMayBeStatic")
    private ModelPublicationDetails convertToModelPublicationDetails(GroovyRowResult r) {
        String uri = r.uri
        def result = new ModelPublicationDetails(uri: uri)
        String linkType = r.linkType
        if (PUBMED == linkType) {
            result.source = ModelPublicationDetails.PublicationSource.MED
            result.referenceType = ModelPublicationDetails.PublicationReferenceType.PMID
        } else {
            result.referenceType = ModelPublicationDetails.PublicationReferenceType.DOI
        }
        result.id = r.publicationId

        result
    }
}
