package net.biomodels.eupmc.links

import groovy.transform.CompileStatic

/**
 * Simple POGO for storing information extracted by {@link ExtractionService} implementations
 */
@CompileStatic
class ModelPublicationDetails {
    /**
     * The uri to the model, typically in identifiers.org form
     */
    String uri
    /**
     * The publication id (the publication link in BioModels)
     */
    String id
    /**
     * The source from which the publication was imported into EU PMC.
     */
    PublicationSource source
    /**
     * The way in which the publication is referenced (e.g. through its PMID or DOI).
     */
    PublicationReferenceType referenceType = null
    /**
     * Basic validation for an instance of this class.
     *
     * @return the list of errors identified. If empty, the instance is deemed valid.
     */
    List<String> validateAndGetErrors() {
        def err = []
        if (null == id || id.isEmpty()) err << 'The publication id is not set'
        if (null == uri || uri.isEmpty()) err << 'The model uri is not set'
        if (null == referenceType) err << "The manuscript reference type is undefined"
        err
    }

    /**
     * Shorthand for calling {@code}validateAndGetErrors().isEmpty()
     *
     * @return true if this instance passws validation
     * @see ModelPublicationDetails#validateAndGetErrors()
     */
    boolean isValid() {
        validateAndGetErrors().isEmpty()
    }

    /**
     * Enum to represent the Source attribute from EUPMC's external links service.
     * See labslink.xsd for the full range of values they support.
     *
     * <p>We currently only look in MEDLINE for retrieving publication details, so for publications
     * that were not linked through their PMID we cannot set the source because we do not know
     * whether DOI/URL/Manual entry publications exist in Europe PMC.
     */
    static enum PublicationSource {
        /**
         * Corresponds to MEDLINE
         */
        MED
    }

    /**
     * Representation of the different ways in which a publication can be referenced.
     *
     * <p>EuropePMC External Links Service only supports references made through PMID, PMCID or DOI.
     * BioModels does not currently support PubMedCentral identifiers (PMCID).
     */
    static enum PublicationReferenceType {
        /**
         * Denotes publications referenced through their PubMed identifier
         */
        PMID,
        /**
         * Denotes publications referenced through their Digital Object Identifier
         */
        DOI
    }
}
