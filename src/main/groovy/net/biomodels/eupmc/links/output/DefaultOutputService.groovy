package net.biomodels.eupmc.links.output

import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic
import net.biomodels.eupmc.links.ModelPublicationDetails
import net.biomodels.eupmc.links.OutputService
import net.biomodels.eupmc.links.config.Configuration
import net.biomodels.eupmc.links.config.Provider

import java.nio.file.Paths

/**
 * Default {@link OutputService} implementation.
 *
 * <p>This service persists the files in the current working directory, overwriting existing content.
 */
@CompileStatic
class DefaultOutputService implements OutputService {
    public static final String PROFILE_XML = 'profile.xml'
    public static final String LINKS_XML   = 'links.xml.gz'

    @Override
    void writeProfileInformation(Configuration config) {
        Provider provider = getValidatedProviderInfo(config)
        try {
            doWriteProfileInfo(provider)
        } catch (Exception e) {
            throw new IllegalStateException("Could not write provider info: $e")
        }
    }

    @SuppressWarnings("GrMethodMayBeStatic")
    private Provider getValidatedProviderInfo(Configuration config) {
        Provider provider = config?.provider
        if (!provider)
            throw new IllegalArgumentException('Provider information missing from the configuration.')
        if (!provider.isValid())
            throw new IllegalStateException('Provider information is not valid.')
        provider
    }

    @CompileDynamic
    private void doWriteProfileInfo(Provider p) throws IOException {
        def body = {
            providers {
                provider() {
                    id(p.id)
                    resourceName(p.name)
                    description(p.description)
                    email(p.email)
                }
            }
        }

        XmlSupport.writeAsXml(body, Paths.get(PROFILE_XML))
    }

    @Override
    void writeLinksInformation(Configuration config, Collection<ModelPublicationDetails> links) {
        Provider provider = getValidatedProviderInfo config
        try {
            doWriteLinksInformation provider, Objects.requireNonNull(links)
        } catch (Exception e) {
            throw new IllegalStateException("Could not persist model publication mapping: $e")
        }
    }

    /**
     * Writes
     * @param p
     * @param _links
     * @throws IOException
     */
    @CompileDynamic
    private void doWriteLinksInformation(Provider p, Collection<ModelPublicationDetails> _links)
            throws IOException {
        def body = {
            links {
                _links.each { l ->
                    if (l.isValid()) {
                        link(providerId: p.id) {
                            resource {
                                url(l.uri)
                            }
                            switch (l.referenceType) {
                                case ModelPublicationDetails.PublicationReferenceType.PMID:
                                    record {
                                        if (l.source) {
                                            source(l.source)
                                        }
                                        id(l.id)
                                    }
                                    break
                                case ModelPublicationDetails.PublicationReferenceType.DOI:
                                    doi(l.id)
                                    break
                                default:
                                    System.err.println "Unsupported reference type '$l.referenceType' for $l"
                                    break
                            }
                        }
                    } else {
                        def err = l.validateAndGetErrors()
                        System.err.println("Not outputing '$l' due to validation errors $err")
                    }
                }
            }
        }

        XmlSupport.writeAsZippedXml body, Paths.get(LINKS_XML)
    }

}
