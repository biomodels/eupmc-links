package net.biomodels.eupmc.links.output

import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic
import groovy.xml.StreamingMarkupBuilder

import java.nio.charset.Charset
import java.nio.charset.StandardCharsets
import java.nio.file.Files
import java.nio.file.Path
import java.util.zip.GZIPOutputStream


/**
 * Convenience methods for writing XML files.
 */
@CompileStatic
class XmlSupport {
    public static final Charset UTF8 = StandardCharsets.UTF_8

    /**
     * Writes the given content out to a file using Groovy's
     *      <a href='http://groovy-lang.org/processing-xml.html#_streamingmarkupbuilder'>StreamingMarkupBuilder</a>
     * @param body  a closure denoting the markup to write.
     * @param out   the output file
     * @throws IOException if an exception if thrown while persisting the file.
     * @see StreamingMarkupBuilder
     */
    static void writeAsXml(Closure body, Path out) throws IOException {
        def writer = Files.newBufferedWriter(out, UTF8)
        try {
            doWriteAsXml(body, writer)
        } finally {
            if (null != writer) {
                writer.flush()
                writer.close()
            }
        }
    }

    /**
     * Create a zipped XML file with the given markup.
     * @param body  the closure denoting the markup to write
     * @param out   the output file
     * @throws IOException  if an exception if thrown while persisting the file.
     * @see StreamingMarkupBuilder
     * @see XmlSupport#writeAsXml(groovy.lang.Closure, java.nio.file.Path)
     */
    static void writeAsZippedXml(Closure body, Path out) throws IOException {
        def fileWriter = createZippedFileWriter out
        try {
            doWriteAsXml(body, fileWriter)
        } finally {
            if (null != fileWriter) {
                fileWriter.flush()
                fileWriter.close()
            }
        }
    }

    /**
     * Creates a {@link Writer} with UTF-8 encoding and GZIP compression.
     *
     * @param out   the output file.
     * @return  a Writer which can be used to write to the given output file.
     * @throws IOException if an exception if thrown while persisting the file.
     */
    static Writer createZippedFileWriter(Path out) throws IOException {
        def stream = new GZIPOutputStream(Files.newOutputStream(out))
        new BufferedWriter(new OutputStreamWriter(stream, UTF8))
    }

    /**
     * Writes the given markup to the indicated output file.
     *
     * @param body  the closure denoting the markup to write
     * @param writer    the buffer where to write this markup
     * @throws IOException  if an exception if thrown while persisting the file.
     * @see StreamingMarkupBuilder
     */
    @CompileDynamic
    private static void doWriteAsXml(Closure body, Writer writer) throws IOException {
        new StreamingMarkupBuilder()
            .bind(body)
            .writeTo(writer)
    }
}
