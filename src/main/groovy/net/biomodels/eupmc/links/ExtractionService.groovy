package net.biomodels.eupmc.links

import net.biomodels.eupmc.links.config.Configuration

/**
 * Service for retrieving model information.
 *
 * Concrete implementations are expected to provide the data that should then be converted to
 * {@link ModelPublicationDetails} and written out as XML by downstream components
 * (i.e. {@link OutputService}.
 */
interface ExtractionService {
    Collection<ModelPublicationDetails> fetch(Configuration config)
}
