package net.biomodels.eupmc.links

import net.biomodels.eupmc.links.config.Configuration

interface OutputService {
    void writeProfileInformation(Configuration config)
    void writeLinksInformation(Configuration config, Collection<ModelPublicationDetails> links)
}
