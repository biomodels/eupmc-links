/**
 * Configuration support.
 *
 * <p>Provides domain classes and logic for parsing settings from externalised
 * properties files.</p>
 */
package net.biomodels.eupmc.links.config;
