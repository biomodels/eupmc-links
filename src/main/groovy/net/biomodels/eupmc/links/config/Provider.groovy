package net.biomodels.eupmc.links.config

import groovy.transform.CompileStatic

/**
 * Stores Provider information for the EuropePMC External Links Service.
 *
 * <pre>
 *   &lt;?xml version="1.0" encoding="UTF-8" standalone="yes"?&gt;
 *   &lt;providers&gt;
 *     &lt;provider&gt;
 *       &lt;id&gt;4580&lt;/id&gt;
 *       &lt;resourceName&gt;Biodiversity Browser&lt;/resourceName&gt;
 *       &lt;description&gt;Species names mined from text and linked to Wikipedia&lt;/description&gt;
 *       &lt;email&gt;jbloggs@biodivres.org&lt;/email&gt;
 *     &lt;/provider&gt;
 *   &lt;/providers&gt;
 */
@CompileStatic
class Provider {
    public static final String PREFIX      = 'provider.'
    public static final String ID          = 'id'
    public static final String NAME        = 'name'
    public static final String DESCRIPTION = 'description'
    public static final String EMAIL       = 'email'
    int id
    String name
    String description
    String email

    static Provider fromProperties(Properties properties) {
        Integer id          = getProviderSetting(ID, properties) as Integer
        String name         = getProviderSetting(NAME, properties)
        String description  = getProviderSetting(DESCRIPTION, properties)
        String email        = getProviderSetting(EMAIL, properties)

        new Provider(id: id, name: name, description: description, email: email)
    }

    private static <T> T getProviderSetting(String shortName, Properties p) {
        ConfigUtil.<T>lookupRequiredSetting(PREFIX, shortName, p) as T
    }

    boolean isValid() {
        id != null && id > 0 &&
            !name?.trim()?.isEmpty() &&
            !description?.trim()?.isEmpty() &&
            email?.trim()?.contains('@')
    }
}
