package net.biomodels.eupmc.links.config

import groovy.transform.CompileStatic

@CompileStatic
class Database {
    public static final String PREFIX   = 'database.'
    public static final String URL      = 'url'
    public static final String USER     = 'user'
    public static final String PASSWORD = 'password'
    public static final String DRIVER   = 'driver'
    private String url
    private String user
    private String password
    private String driver

    static Database fromProperties(Properties properties) {
        String url      = getDbSetting(URL, properties)
        String user     = getDbSetting(USER, properties)
        String pass     = getDbSetting(PASSWORD, properties)
        String driver   = getDbSetting(DRIVER, properties)
        new Database(url: url, user: user, password: pass, driver: driver)
    }

    private static String getDbSetting(String name, Properties properties) {
        ConfigUtil.lookupRequiredSetting(PREFIX, name, properties)
    }

    Map<String, Object> asSqlMap() {
        Map<String, Object> result = [:]
        result.url      = url
        result.user     = user
        result.password = password
        result.driver   = driver
        result
    }
}
