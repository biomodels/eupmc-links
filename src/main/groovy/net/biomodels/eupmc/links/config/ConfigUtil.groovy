package net.biomodels.eupmc.links.config

import groovy.transform.CompileStatic

@CompileStatic
final class ConfigUtil {
    static <T> T lookupRequiredSetting(String prefix, String name, Properties props) {
        String key = prefix + name
        if (!props.containsKey(key)) {
            throw new IllegalArgumentException("Configuration setting '$key' is not present.")
        }

        //noinspection unchecked
        return (T) props.getProperty(key)
    }
}
