package net.biomodels.eupmc.links.config

import groovy.transform.CompileStatic

import java.nio.charset.StandardCharsets
import java.nio.file.Files
import java.nio.file.Path

@CompileStatic
class Configuration {
    Provider provider
    Database database

    static Configuration parseSettings(Path configLocation) {
        if (null == configLocation || !Files.isRegularFile(configLocation)) {
            throw new IllegalArgumentException("External configuration file expected but not found")
        }
        Properties p = new Properties()
        def reader = new InputStreamReader(new FileInputStream(configLocation.toFile()),
                StandardCharsets.UTF_8)
        p.load new BufferedReader(reader)

        def provider = Provider.fromProperties(p)
        def database = Database.fromProperties(p)

        new Configuration(provider: provider, database: database)
    }
}
