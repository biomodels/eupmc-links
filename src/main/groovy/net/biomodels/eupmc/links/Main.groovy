package net.biomodels.eupmc.links

import groovy.transform.CompileStatic

@CompileStatic
class Main {
    static void main(String[] args) {
        try {
            def result = new ModelPublicationMapper(args).run()
            println "execution complete:  ${result ? 'ok' : 'failed' }"
        } catch (Throwable t) {
            System.err.println("Fatal error: $t")
        }
    }
}
