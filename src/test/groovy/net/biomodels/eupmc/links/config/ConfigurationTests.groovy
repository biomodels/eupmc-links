package net.biomodels.eupmc.links.config

import spock.lang.Specification

import java.nio.file.Paths

class ConfigurationTests extends Specification {

    void "undefined configuration paths are rejected"() {
        when:
        Configuration.parseSettings(null)
        then:
        thrown IllegalArgumentException

        when:
        def nonExistentPath = Paths.get("/nowhere")
        Configuration.parseSettings nonExistentPath
        then:
        !nonExistentPath.toFile().exists()
        thrown IllegalArgumentException
    }

    void "can load a valid properties file"() {
        when:
        def sampleConfigPath = Paths.get("src", "test", "resources", "sample.properties")
        def cfg = Configuration.parseSettings sampleConfigPath
        then:
        sampleConfigPath.toFile().exists()
        notThrown IllegalArgumentException
        cfg
        cfg.database.asSqlMap().url == 'jdbc:h2:mem:foo'
    }

    void "missing settings cause IllegalArgumentException"() {
        when:
        Configuration.parseSettings(Paths.get("src", "test", "resources", "incomplete.properties"))
        then:
        thrown IllegalArgumentException
    }
}
