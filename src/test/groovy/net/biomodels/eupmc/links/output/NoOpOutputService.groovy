package net.biomodels.eupmc.links.output

import net.biomodels.eupmc.links.ModelPublicationDetails
import net.biomodels.eupmc.links.OutputService
import net.biomodels.eupmc.links.config.Configuration

class NoOpOutputService implements OutputService{
    @Override
    void writeProfileInformation(Configuration config) {
        // nothing
    }

    @Override
    void writeLinksInformation(Configuration config, Collection<ModelPublicationDetails> links) {
        // nothing
    }
}
