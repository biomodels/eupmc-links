package net.biomodels.eupmc.links.output

import net.biomodels.eupmc.links.ModelPublicationDetails
import net.biomodels.eupmc.links.config.Configuration
import net.biomodels.eupmc.links.config.Provider
import spock.lang.Specification

import java.nio.charset.StandardCharsets
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.util.zip.GZIPInputStream

class OutputServiceTests extends Specification {

    def cleanup() {
        deleteOrLogFailure(Paths.get(DefaultOutputService.PROFILE_XML))
        deleteOrLogFailure(Paths.get(DefaultOutputService.LINKS_XML))
    }

    private static boolean deleteOrLogFailure(Path p) {
        if (!Files.exists(p)) return true // NOTHING TO DO
        def result = false
        try {
            result = Files.deleteIfExists(p)
        } catch (IOException e) {
            if (Files.exists(p)) {
                System.err.println("File $p could not be deleted due to $e")
            }
            result = false
        }
        result
    }

    void 'writeProfileInfo expects a valid provider'() {
        when:
        def service = new DefaultOutputService()
        def c = new Configuration(provider: null)
        service.writeProfileInformation(c)
        then:
        thrown IllegalArgumentException

        when:
        c = new Configuration(provider: new Provider())
        service.writeProfileInformation c
        then:
        thrown IllegalStateException
    }

    void 'writeProfileInfo acceptance test'() {
        when:
        def service = new DefaultOutputService()
        def c = Configuration.parseSettings(Paths.get("src/test/resources/sample.properties"))
        service.writeProfileInformation c
        def out = Paths.get(DefaultOutputService.PROFILE_XML)

        then:
        noExceptionThrown()
        Files.isRegularFile out
        out.toFile().text == """\
<providers><provider><id>1234</id><resourceName>Randomodels</resourceName>\
<description>A random collection of models that serve no purpose whatsoever.</description>\
<email>test@example.com</email></provider></providers>"""
    }

    void "writeLinksInfo rejects invalid provider information"() {
        when:
        def service = new DefaultOutputService()
        def c = new Configuration(provider: null)
        service.writeLinksInformation c, []
        then:
        thrown IllegalArgumentException

        when:
        c = new Configuration(provider: new Provider())
        service.writeLinksInformation c, []
        then:
        thrown IllegalStateException
    }

    void "writeLinksInfo ignores invalid links"() {
        when:
        def service = new DefaultOutputService()
        def c = Configuration.parseSettings(Paths.get("src/test/resources/sample.properties"))
        def links = [
            new ModelPublicationDetails(), // this link should be ignored,
            new ModelPublicationDetails(uri: 'http://ddg.gg',
                referenceType: ModelPublicationDetails.PublicationReferenceType.PMID,
                source: ModelPublicationDetails.PublicationSource.MED, id: '12344321')
        ]
        service.writeLinksInformation(c, links)
        def out = Paths.get(DefaultOutputService.LINKS_XML)

        then:
        noExceptionThrown()
        readZippedFile(out) == """\
<links><link providerId='1234'>\
<resource><url>http://ddg.gg</url></resource>\
<record><source>MED</source><id>12344321</id></record>\
</link></links>"""
    }

    void 'writeLinksInfo acceptance test for PUBMED and DOI entries'() {
        when:
        def service = new DefaultOutputService()
        def c = Configuration.parseSettings(Paths.get("src/test/resources/sample.properties"))
        def pmid = new ModelPublicationDetails(uri: 'http://ddg.gg',
            referenceType: ModelPublicationDetails.PublicationReferenceType.PMID,
            source: ModelPublicationDetails.PublicationSource.MED, id: '12344321')
        def doi  = new ModelPublicationDetails(uri: 'http://example.com/MODEL1234',
            referenceType: ModelPublicationDetails.PublicationReferenceType.DOI,
            id: '10.1021/pr100637q')
        service.writeLinksInformation(c, [pmid, doi])
        def out  = Paths.get(DefaultOutputService.LINKS_XML)

        then:
        noExceptionThrown()
        readZippedFile(out) == """\
<links><link providerId='1234'>\
<resource><url>http://ddg.gg</url></resource>\
<record><source>MED</source><id>12344321</id></record>\
</link>\
<link providerId='1234'>\
<resource><url>http://example.com/MODEL1234</url></resource>\
<doi>10.1021/pr100637q</doi></link>\
</links>"""
    }

    private static String readZippedFile(Path p) {
        def stream = new GZIPInputStream(Files.newInputStream(p))
        def reader = new BufferedReader(new InputStreamReader(stream, StandardCharsets.UTF_8))
        try {
            reader.readLines().join('\n')
        } finally {
            reader?.close()
        }
    }
}
