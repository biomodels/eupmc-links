package net.biomodels.eupmc.links

import net.biomodels.eupmc.links.config.Configuration
import net.biomodels.eupmc.links.output.NoOpOutputService
import net.biomodels.eupmc.links.retrieval.DefaultExtractionService
import net.biomodels.eupmc.links.retrieval.DummyExtractionService
import spock.lang.Specification

class ModelPublicationMapperTests extends Specification {

    public static final String[] acceptanceCLIArguments =
            ['src/test/resources/sample.properties'].toArray(new String[1])

    void 'constructing an instance with no args causes an exception'() {
        when:
        new ModelPublicationMapper()
        then:
        thrown IllegalArgumentException

        when:
        new ModelPublicationMapper(new String[0], new DummyExtractionService(), new NoOpOutputService())
        then:
        thrown IllegalArgumentException
    }

    void "extractionService exceptions get caught by the model publication mapper"() {
        when:
        def mapper = new ModelPublicationMapper(acceptanceCLIArguments,
                new ExceptionThrowingExtractionService(), new NoOpOutputService())
        def result = mapper.run()
        then:
        !result
        notThrown Exception
    }

    void 'acceptance test'() {
        given:
        def extractionService = new DummyExtractionService()
        extractionService.expectedDetails = [ new ModelPublicationDetails() ]
        when:
        def mapper = new ModelPublicationMapper(acceptanceCLIArguments,
            extractionService, new NoOpOutputService())
        then:
        mapper.run()
    }
}

class ExceptionThrowingExtractionService implements ExtractionService {

    @Override
    Collection<ModelPublicationDetails> fetch(Configuration config) {
        throw new Exception("I refuse to fetch publication details, do it yourself")
    }
}
