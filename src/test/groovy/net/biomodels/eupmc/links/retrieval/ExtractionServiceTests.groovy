package net.biomodels.eupmc.links.retrieval

import net.biomodels.eupmc.links.ExtractionService
import net.biomodels.eupmc.links.config.Configuration
import spock.lang.Ignore
import spock.lang.Specification

import java.nio.file.Paths

class ExtractionServiceTests extends Specification {

    void 'exceptions caused by database work are thrown as RuntimeExceptions'() {
        when:
        def c = Configuration.parseSettings(Paths.get('src/test/resources/badDriver.properties'))
        def results = new DefaultExtractionService().fetch(c)
        then:
        thrown(RuntimeException)
    }

    @Ignore('the test database has no tables, hence DefaultExtractionService queries do not work')
    // TODO add fixtures to test database in order for this test to pass
    void "acceptance test for extraction service"() {
        when:
        def c = Configuration.parseSettings(Paths.get('src/test/resources/sample.properties'))
        ExtractionService service = new DefaultExtractionService()
        def results = service.fetch c

        then: 'we have at least one result'
        results.size() > 0
        results.first().uri.startsWith('http://identifiers.org/biomodels.db/')

        and: 'the results are valid'
        !results.find { !it.isValid() }
    }
}
