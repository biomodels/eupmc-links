package net.biomodels.eupmc.links.retrieval;

import net.biomodels.eupmc.links.ExtractionService;
import net.biomodels.eupmc.links.ModelPublicationDetails;
import net.biomodels.eupmc.links.config.Configuration;

/**
 * Trivial {@linkplain ExtractionService} implementation which allows us to specify the result
 * that should be returned by its <strong>fetch()</strong> call.
 */
class DummyExtractionService implements ExtractionService {
    Collection<ModelPublicationDetails> expectedDetails

    @Override
    Collection<ModelPublicationDetails> fetch(Configuration config) {
        expectedDetails
    }
}
